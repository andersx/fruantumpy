#!/usr/bin/env python
"""
**********************************************************************
avg

Copyright (C) 2012 Casper Steinmann

This file is part of the quantumpy project.

FragIt is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

FragIt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.
***********************************************************************/
"""
import sys
import os

import numpy

from qm import Gamess
from qm import Mopac
from minimizers import ConjugateGradient
from qm.units import UNITS, MASS
from util import *

def perturb_coords(c,iat,idx,a):
  (nat,n) = numpy.shape(c)
  c_cor = numpy.zeros((nat,n))
  c_cor[iat][idx] = a*UNITS['AANGSTROM/AU']
  return c[:]+c_cor[:]

def getPerturbedGradient(method,i,j,a):
  org_coords = method.getCoordinates()
  c = perturb_coords(org_coords,i,j,a)
  method.setCoordinates(c)
  G = numpy.array(method.getGradient()) # this is needed or the gradient is overwritten - f*ck you python
  method.setCoordinates(org_coords)
  return G

def getPerturbedGradientOfNormalMode(method,i,mode,a):
  org_coords = method.getCoordinates()
  c = org_coords[:]
  c[i] += mode*a*UNITS['AANGSTROM/AU']
  method.setCoordinates(c)
  G = numpy.array(method.getGradient()) # this is needed or the gradient is overwritten - f*ck you python
  method.setCoordinates(org_coords)
  return G

def symmetrize(K):
  (n,n1) = numpy.shape(K)
  KK = K[:]#numpy.zeros((n,n1)) K[:]
  for i in range(n):
    for j in range(n):
      if i<j:
        KK[j][i] = K[i][j]
  return KK

def getNormalCoordinates(C,L):
  temp = numpy.dot(C,L.transpose())
  return numpy.dot(L,temp)

def massWeighHessian(H,mol):
  M = H[:]
  types = OBAtomTypes(mol)
  for atom1 in range(mol.NumAtoms()):
    for atom2 in range(mol.NumAtoms()):
      #if atom2 == atom1: continue
      mass1 = MASS[types[atom1]]
      mass2 = MASS[types[atom2]]
      RMASS = 1.0 / numpy.sqrt(mass1*mass2)
      #print atom1, atom2,mass1,mass2,RMASS

      # just a very dumb loop to mass weight H correctly. Could probably be done in a
      # more clever (but infinitely more difficult) way using numpy.
      # make small 3x3 loops based on atoms
      for i in range(atom1*3, 3*(atom1+1)):
        for j in range(atom2*3, 3*(atom2+1)):
          M[i,j] /= RMASS

  return M

if __name__ == '__main__':
  filename = sys.argv[1]
  mol = fileToMol(filename)
  method = Gamess(mol)
  #method = Mopac(mol)
  #method.setBasisSet('PM3')
  method.setBasisSet('6-31G(d)')
  #method.setBasisSet('3-21G')
  method.setDFTFunctional('M06')
  print
  print "---------------------"
  print "minimizing input geom"
  print "---------------------"
  print
  cg = ConjugateGradient(method, 1.e-9)
  cg.solve()
  cc = method.getCoordinates()
  a = 0.075 #*UNITS['AANGSTROM/AU']
  (nat,n) = (mol.NumAtoms(), 3)
  K = numpy.zeros((nat*n,nat*n))
  idx = 0
  print
  print "---------------------"
  print "calculating hessian  "
  print "---------------------"
  print
  for i in range(nat):
    for j in range(n):
      idx = 3*i+j
      print " * calculating gradient (%2i/%2i)." % (idx+1,nat*n)
      gp = getPerturbedGradient(method,i,j, 1.0*a)
      gm = getPerturbedGradient(method,i,j,-1.0*a)
      dg = numpy.ravel(gp - gm) / 2.0
      K[idx][idx:] = dg[idx:]

  K = symmetrize(K)
  K = K / a  # make the Hessian the correct units [hartree/bohr^2]

  # as in GAMESS, we print the bare hessian
  print
  print "---------------------"
  print "force constant matrix"
  print "---------------------"
  print
  for i in range(nat*n):
    for j in range(nat*n):
      print "%10.6f" % K[i,j],
    print

  # we find eigenvalues and eigenvectors from the mass weighed hessian
  from numpy.linalg import eig
  K = massWeighHessian(K, mol)
  #K = K / (MASS[1])  # mass weighing for h2
  (W,V) = eig(K)
  indices = numpy.argsort(W)
  Ws = W[indices]
  Vs = V.transpose()[indices] # funny arrangement of the eigenvectors

  #Ws = Ws[numpy.where(Ws<0.0,0.0,Ws)]
  Ws[numpy.where(Ws<0.0)] = 0.0 # negative numbers are forced zero
  Ws2 = numpy.sqrt(Ws)
  Ws = numpy.sqrt(Ws * 2.642461e+07)

  print
  print "---------------------"
  print "    eigen modes"
  print "---------------------"
  print
  for i,eigenmode in enumerate(Vs):
    print "mode %2i, f = %9.2f Hz:" % (i+1,Ws[i]),
    for value in eigenmode:
      print "%16.9f" % value,
    print

  # hardcode for H2
  nstart = 5 # if linear, nstart is 5 otherwise nstart is 6
  nstart = 5
  nend = len(Vs)
  corrections = []
  for nmode in range(nstart, nend):
    mode = numpy.reshape(Vs[nmode], (method.getNumAtoms(),3))
    print
    print "displacing along normal mode %3i" % (nmode+1)
    #print "mode:", mode
    grads = []
    for i,direction in enumerate(mode):
      grads.append( getPerturbedGradientOfNormalMode(method,i,direction,a) )

    Vimm = sum(grads)/(a*UNITS['AANGSTROM/AU'])**2
    print "VIMM:", numpy.ravel(Vimm)
    #print Ws[nmode]
    corr = Vimm[0][0] / Ws2[nmode]#numpy.sum(Vimm) / Ws2[nmode]
    #corr = 0.25 / (Ws2[nmode]**2) * Vimm[0][0] / Ws2[nmode]
    print "corr: %16.9f" % (corr)
    corrections.append( corr )

  print
  #print corrections

  R = cc[1][0] - cc[0][0]
  correction = (0.25 / Ws2[nmode]**2) * sum(corrections)

  print "vibrational correction to bond length is"
  print "    R(0)               = %16.9f" % R
  print "    R(1)               = %16.9f" % correction
  print "    R(A) = R(0) - R(1) = %16.9f" % (R - correction)
  print
