"""
**********************************************************************
conjugategradient.py

Copyright (C) 2012 Casper Steinmann

This file is part of the quantumpy project.

FragIt is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

FragIt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.
***********************************************************************/
"""
import numpy

class ConjugateGradient(object):
  def __init__(self,method,eps=1.0e-4, printout=True):
    self.method = method
    self.eps = eps
    (self.e0,self.g0) = self.method.getEnergyAndGradient()
    self.d0 = -self.g0
    self.a = 0.3
    self.printout = printout

  def solve(self):
    count = 0
    while(1):
      self.step()
      norm = numpy.dot( numpy.ravel(self.g0), numpy.ravel(self.g0) )
      if self.printout: print "I=%4i  E=%16.7f  Gnorm=%14.9f" % (count, self.e0, norm)
      if norm < self.eps:
        break
      count += 1

  def step(self):
    c = self.method.getCoordinates()
    c1 = c + self.a*self.d0
    self.method.setCoordinates(c1)
    (e1,g1) = self.method.getEnergyAndGradient()
    d1 = -g1 
    b = numpy.dot( numpy.ravel(d1), numpy.ravel(d1-self.g0) ) / numpy.dot( numpy.ravel(self.d0), numpy.ravel(self.d0) )

    self.e0 = e1
    self.g0 = g1
    self.d0 = d1 + b*self.d0
