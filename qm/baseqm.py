"""
**********************************************************************
baseqm.py

Copyright (C) 2012 Casper Steinmann

This file is part of the quantumpy project.

FragIt is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

FragIt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.
***********************************************************************/
"""
import os
import copy
import openbabel
import numpy

from util import OBGetCoordinates, OBSetCoordinates, molToFile

class BaseQM(object):
  def __init__(self,obmol):
    self.obmol = obmol
    self._setupDefaults()
    self._computePartialAtomicCharges()
    self._setupStandards()

  def _setupDefaults(self):
    self.changed = True
    self.e = 0.0
    self.g = numpy.zeros((self.getNumAtoms(),3))
    try:
      temporary_dir = os.getenv['QUANTUMPYTEMP']
    except TypeError:
      self.temporary_dir = '/tmp' # default is /tmp
    self.base_file = 'temp'

  def _computePartialAtomicCharges(self):
    self._charge_model = openbabel.OBChargeModel.FindType("mmff94")
    self._charge_model.ComputeCharges(self.obmol)

  def _setupStandards(self):
    self.path = '' # path to executable
    self.infile_extension = '' # file extension on input file
    self.outfile_extension = '' # file extension on output file
    self.obformat = '' # Open Babel format - gamin for example
    self.command = '' # command to execute the program of interest

  def _parseLogfile(self):
    raise NotImplementedError("You must use a derived class of BaseQM")

  def _recalculateQM(self):
    cwd = os.getcwd()
    os.chdir(self.temporary_dir)
    if len(self.command) != 0:
      self._makeInput()
      os.system(self.command % (self.path, self.infile, self.outfile))
      self._parseLogfile()
      self.changed = False
    os.chdir(cwd)

  def _makeInput(self):
    self._makeHeader()
    self.infile  = '%s.%s' % (self.base_file, self.infile_extension)
    self.outfile = '%s.%s' % (self.base_file, self.outfile_extension)
    molToFile(self.obmol,self.infile,'%s/%s' % (self.temporary_dir,self.obformat))

  def _makeHeader(self):
    raise NotImplementedError("You must use a derived class of BaseQM")

  def getEnergy(self):
    if self.changed: self._recalculateQM()
    return self.e

  def getGradient(self):
    if self.changed: self._recalculateQM()
    return self.g[:]

  def getEnergyAndGradient(self):
    if self.changed: self._recalculateQM()
    return (self.e, self.g[:])

  def getNumAtoms(self):
    return self.obmol.NumAtoms()

  def getCoordinates(self):
    return OBGetCoordinates(self.obmol)

  def setCoordinates(self, newcoords):
    self.obmol = OBSetCoordinates(self.obmol, newcoords)
    self.changed = True

  def isOK(self):
    value = self.getAtomCount() > 1
    return value

  def MatchPattern(self, pattern_to_match):
    self._pattern.Init(pattern_to_match)
    self._pattern.Match(self.obmol)
    match = [m for m in self._pattern.GetUMapList()]
    return match

  def getPartialAtomicCharges(self):
    if self.changed: self._computePartialAtomicCharges()
    return self._charge_model.GetPartialCharges()

  def getTotalCharge(self):
    return int(sum(self.getPartialAtomicCharges()))

  def getElementSymbol(self,atom_index):
    table = openbabel.OBElementTable()
    return table.GetSymbol(atom_index)

  # method setting stuff, basis set and so forth
  def setBasisSet(self, basis):
    if self._basisSets.has_key(basis):
      self.basis = self._basisSets[basis]

  def setDFTFunctional(self, functional):
    if self._functionals.has_key(functional):
      self.functional = self._functionals[functional]
