"""
**********************************************************************
gamess.py

Copyright (C) 2012 Casper Steinmann

This file is part of the quantumpy project.

FragIt is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

FragIt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
02110-1301, USA.
***********************************************************************/
"""
from baseqm import BaseQM

# BASIS SET DEFINITION
BASIS = dict()
BASIS['PM3'] = 'PM3'
BASIS['3-21G'] = 'N21 NGAUSS=3'
BASIS['6-31G(d)'] = 'N31 NGAUSS=6 NDFUNC=1'

FUNCTIONALS = dict()
FUNCTIONALS['B3LYP'] = 'B3LYP'
FUNCTIONALS['M06'] = 'M06'


HEADER = """ $system mwords=125 $end
 $basis gbasis=%s $end
 $contrl runtyp=gradient dfttyp=%s
         icharg=%i
 $end
"""

class Gamess(BaseQM):
  def __init__(self,obmol):
    BaseQM.__init__(self,obmol)
    self._basisSets = BASIS
    self._functionals = FUNCTIONALS
  
  def _setupStandards(self):
    self.path = "/home/cstein/Development/fortran/GAMESS_git/rungms"
    self.infile_extension = 'inp'
    self.outfile_extension = 'log'
    self.obformat = 'gamin'
    self.command = '%s %s 2> /dev/null > %s'

  def _makeHeader(self):
    try:
      basis = self.basis
    except AttributeError:
      raise AttributeError("Basis set not set")
    try:
      functional = self.functional
    except AttributeError:
      functional = "NONE"
    f = open("%s/%s" % (self.temporary_dir, self.obformat), "w")
    f.write(HEADER % (basis, functional, self.getTotalCharge()))
    f.close()


  def _parseLogfile(self):
    f = open(self.outfile, 'r')
    counter = 0
    for line in f:
  
      # parse the energy
      if "FINAL" in line:
        data = line.split()
        self.e = float(data[4])
  
      # parse the gradient
      if "                         GRADIENT OF THE ENERGY" in line:
        counter = 4+self.getNumAtoms()
  
      if counter > 0:
        counter -= 1
        if counter < self.getNumAtoms():
          idx = self.getNumAtoms() - counter -1
          data = line.split()
          iat_grad = map(float, data[2:])
          self.g[idx] = iat_grad[:]
  
    f.close()

if __name__ == '__main__':
  pass
